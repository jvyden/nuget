﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text.Json;

using Builder;
using Fluid;
using GitLabApiClient;
using Newtonsoft.Json.Linq;
using NuGet.CatalogReader;
using NuGet.Protocol;

static async Task<T> ReadConfigAsync<T>(string filePath)
{
    using FileStream stream = File.OpenRead(filePath);
    return await JsonSerializer.DeserializeAsync<T>(stream);
}


static async Task RunCommand(params string[] command)
{
    var c = string.Join(' ', command);
    Console.WriteLine("running command {0}", c);
    var process = new Process()
    {
        StartInfo = new ProcessStartInfo()
        {
            FileName = command.First(),
            Arguments = string.Join(' ', command.Skip(1)),
            RedirectStandardOutput = false,
            RedirectStandardError = false,
            UseShellExecute = true,
        }
    };

    process.Start();

    await process.WaitForExitAsync();

}

if (File.Exists("./.env"))
{
    foreach (var line in File.ReadAllLines("./.env"))
    {
        var parts = line.Split(
            '=',
            StringSplitOptions.RemoveEmptyEntries);

        if (parts.Length != 2)
            continue;
        Console.WriteLine(parts[0]);
        Environment.SetEnvironmentVariable(parts[0], parts[1]);
    }
}
var sourceConfig = await ReadConfigAsync<List<SourceConfig>>(@"./sources.json");


foreach (var item in sourceConfig)
{
    Console.WriteLine("pulling {0}", item.name);
    var feedBuilder = new UriBuilder(item.uri)
    {
        Password = Environment.ExpandEnvironmentVariables(item.password),
        UserName = Environment.ExpandEnvironmentVariables(item.username)
    };

    var feed = feedBuilder.Uri;

    using var catalog = new FeedReader(feed);
    foreach (var package in item.packages)
    { 
        foreach (var entry in await catalog.GetPackagesById(package))
        {
            Console.WriteLine($"{entry.Id} {entry.Version}");
            await entry.DownloadNupkgAsync("./packages", DownloadMode.OverwriteIfNewer, CancellationToken.None);
        }
    }
}

if(Directory.Exists("public"))
    Directory.Delete("public", true);

await RunCommand("/root/.dotnet/tools/sleet", "init", "--config", "./sleet.json", "--source", "LocalFeed", "--with-symbols", "--with-catalog");
await RunCommand("/root/.dotnet/tools/sleet", "feed-settings", "--set", "badgesenabled:true");
await RunCommand("/root/.dotnet/tools/sleet", "recreate");
await RunCommand("/root/.dotnet/tools/sleet", "push", "-s", "LocalFeed", "./packages");


var parser = new FluidParser();

var packages = JObject.Parse(File.ReadAllText("./public/sleet.packageindex.json"));

var model = new { 
    root_uri = Environment.GetEnvironmentVariable("CI_PAGES_URL"),
    packages = packages["packages"].ToObject<JObject>().Properties().Select(p => p.Name).ToList(),
    lastUpdate = DateTime.UtcNow.ToString(new CultureInfo("en-AU"))
};
var source = File.ReadAllText("./template/index.html");

if (parser.TryParse(source, out var template, out var error))
{   
    var context = new TemplateContext(model);
    File.WriteAllText("./public/index.html", template.Render(context));
}
else
{
    Console.WriteLine($"Error: {error}");
}



